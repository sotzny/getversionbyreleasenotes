using System;
using System.Linq;
using Nuke.Common;
using Nuke.Common.BuildServers;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.EnvironmentInfo;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

[CheckBuildProjectConfigurations]
[UnsetVisualStudioEnvironmentVariables]
class Build : NukeBuild
{
    /// Support plugins are available for:
    ///   - JetBrains ReSharper        https://nuke.build/resharper
    ///   - JetBrains Rider            https://nuke.build/rider
    ///   - Microsoft VisualStudio     https://nuke.build/visualstudio
    ///   - Microsoft VSCode           https://nuke.build/vscode

    public static int Main() => Execute<Build>(x => x.Compile);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    GitLab Gitlab = GitLab.Instance;

    // ReSharper disable once InconsistentNaming
    [Parameter("API Key f�r Nuget Push")] readonly string NUGET_APIKEY = "";
    // ReSharper disable once InconsistentNaming
    [Parameter("Nuget Feed URL")] readonly string NUGET_URL = "";

    [Solution] readonly Solution Solution;
    [GitRepository] readonly GitRepository GitRepository;
    bool IsMasterBranch => GitRepository.Branch == "master";

    const string NameNugetLib = "GetVersionByReleaseNotesLib";
    Project NugetProjekt => Solution.GetProject(NameNugetLib).NotNull();

    AbsolutePath SourceDirectory => RootDirectory / "src";
    AbsolutePath TestsDirectory => RootDirectory / "tests";
    AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";
    AbsolutePath NugetOutputPath => ArtifactsDirectory / "NuGet";

    Target Clean => _ => _
        .Before(Restore)
        .Executes(() =>
        {
            SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            TestsDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            EnsureCleanDirectory(ArtifactsDirectory);
        });

    Target Restore => _ => _
        .Executes(() =>
        {
            DotNetRestore(s => s
                .SetProjectFile(Solution));
        });

    Target Compile => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .EnableNoRestore());
        });

    Target Pack => _ => _
        .DependsOn(Compile)
        .Description("Erstellt das Nuget Paket")
        .Executes(() =>
        {
            DotNetPack(s => s
                .SetIncludeSymbols(true)
                .SetPackageProjectUrl(Gitlab?.ProjectUrl ?? "")
                .AddAuthors("Danny Sotzny")
                .SetOutputDirectory(NugetOutputPath)
                .SetConfiguration(Configuration)
                .SetProject(NugetProjekt)
            );
        });

    Target Push => _ => _
        .DependsOn(Pack)
        .Description("Ver�ffentlicht das Nuget")
        .Requires(() => NUGET_APIKEY)
        .Requires(() => NUGET_URL)
        .Requires(() => IsMasterBranch)
        .Executes(() =>
        {
            GlobFiles(NugetOutputPath, "*.nupkg")
                .Where(x => !x.EndsWith(".symbols.nupkg")).NotEmpty()
                .ForEach(x => DotNetNuGetPush(s => s
                    .SetTargetPath(x)
                    .SetSource(NUGET_URL)
                    //.SetSymbolSource("https://nuget.smbsrc.net/")
                    .SetApiKey(NUGET_APIKEY)));
            
        });
}
